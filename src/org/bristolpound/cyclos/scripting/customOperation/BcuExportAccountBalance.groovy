/**
 *
 * Generates a list of accounts, with the balance at a selected point in time.
 *
 * Script parameters:
 *		none
 *
 * Included libraries:
 *		- libErrorHandling
 *
 * Custom form fields:
 *		- Date			dateTo		Optional	Date part of point in time for the snapshot.
 *												Null yields in current date.
 *		- String		timeTo		Optional	Time part of the snapshot in the format
 *												"HH:mm".
 *												Null yields in "23:59".
 *												Seconds are always "59".
 *		- List			groups		Required	List of group's internal names.
 *		- List			fields		Required	List of [index, name, activationDate, account, type,
 *												active, transIn, totalIn, transOut, totalOut, transTotal,
 *												balance]
 *
 * Returns
 *		org.cyclos.model.system.operations.CustomOperationPageResult:
 *		//int			#			Optional	Line number
 *		- String		Member		Default		Username of the account owner.
 *		- String		Activation Date Optional User activation date
 *		- String 		Account		Optional	Account id
 *		- String 		Type		Default		Account type
 *		- Boolean		Active		Optional	Account active [0|1]
 *		- Int			In #		Optional	No. of incoming transactions
 *		- BigDecimal	In £B		Optional	Sum of incoming transactions up to the given point in time
 *		- Int			Out #		Optional	No. of outgoing transactions
 *		- BigDecimal	Out £B		Optional	Sum of incoming transactions up to the given point in time
 *		- Int			Total #		Optional	Total no. of transactions
 *		- BigDecimal	Balance		Default		Account balance at the given point in time
 *												(see fields).
 *
 * Version:
 *		1.2, 2017-01-24
 *
 * Authors:
 *		- martin.rueegg@bristolpound.org
 *
 * Known issues:
 *		- CustomOperationPageContext not working (Cyclos)
 *
 **/


 //import
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.sql.SQLExpressions;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils;

import org.cyclos.entities.banking.QTransfer;
import org.cyclos.entities.banking.QUserAccount;
import org.cyclos.entities.system.CustomFieldPossibleValue;
import org.cyclos.entities.users.UserGroup;
import org.cyclos.impl.utils.persistence.DBQuery;
import org.cyclos.model.users.groups.UserGroupVO;
import org.cyclos.model.utils.DatePeriodDTO;
import org.cyclos.model.utils.ModelHelper;
import org.cyclos.server.utils.DateHelper;


info.addError( 1, "formParameters.userStartsWith " + formParameters.userStartsWith);
info.addError( 1, "isCSV " + isCSV);
info.addError( 1, "isPDF " + isPDF);
info.addError( 1, "isPAGE " + isPAGE);
info.addError( 1, "currentPage" + currentPage);
info.addError( 1, "pageSize" + pageSize);
info.addError( 1, "pageContext " + binding.pageContext);
// return info;

// prepare variables
def int			i 		= 0;


// evaluate selected groups
Boolean				allGroups	= false;
List<UserGroupVO>	groups		= [];

for (CustomFieldPossibleValue cf : formParameters.groups)
{
    if (allGroups == false)
    {
        if (cf.internalName.trim() == "all")
        {
            allGroups = true;
			groups		= [];
			
			def queryBG = new org.cyclos.model.users.groups.BasicGroupQuery([
				groupSet: 		new org.cyclos.model.users.groups.GroupSetVO([internalName: "memberAccounts"]),
				natures:		[org.cyclos.model.users.groups.BasicGroupNature.MEMBER_GROUP],
				currentPage:	0,
				pageSize:		Integer.MAX_VALUE,
			])
			
			info.addError( 1, "pre r ");
			
			groupService.search(queryBG).getPageItems().each
			{	
				r ->
				info.addError( 1, "r " + r.internalName);
				
			//	groups		<<r.clone(); //--> causes an error in accessing an non-persistent object ...
				groups		<<entityManagerHandler.find(UserGroup, r.internalName);
			}
        }
        else
        {
			groups		<<entityManagerHandler.find(UserGroup, cf.internalName.trim());
        }
    }
}

// evaluate selected fields
Map<String, CustomFieldPossibleValue> fields	= new HashMap<String, CustomFieldPossibleValue>();

for (CustomFieldPossibleValue cf : formParameters.fields)
{
	fields.put(cf.internalName, cf);
}

if (fields.size() == 0)
{
	return new ResultError(200, "At least one output field needs to be selected.");
}


// evaluate snapshot point in time (date and time)
if (formParameters.dateTo == null)
	formParameters.dateTo = new Date();

if (formParameters.timeTo == null)
	formParameters.timeTo = "23:59";

def Date		dateTo	= DateUtils.parseDate(formParameters.timeTo, "HH:mm");
def int			iMin	= (int) DateUtils.getFragmentInMinutes(
									DateUtils.parseDate(formParameters.timeTo, "HH:mm"), 
									Calendar.YEAR
									);
									
if (iMin - 1440 >= 1440)
	return new ResultError(300, "Time may not be greater than '23:59' ("+(iMin - 1440)+").");

				dateTo	= DateUtils.addSeconds(
							DateUtils.addMinutes(
								formParameters.dateTo, 
								(int) DateUtils.getFragmentInMinutes(
									dateTo, 
									Calendar.DATE
									)
								),
							59);

info.addError( 1, "dateTo " + dateTo);

DatePeriodDTO period = ModelHelper.datePeriod(
		null,
		conversionHandler.toDateTime(dateTo));

// return info;

def ua		= QUserAccount.userAccount
def t		= QTransfer.transfer
// def tIn		= new QTransfer("tIn")
// def tOut	= new QTransfer("tOut")

class ResultCount {
    Long id
}


DBQuery queryUA = entityManagerHandler
			.from(ua)
			.where(
				  ua.user().group().in(groups)
				, ua.user().activationDate.lt(dateTo)
				// , ua.creationDate.lt(dateTo)
			)

List<ResultCount> resultCount = queryUA
			.select(
				Projections.bean(
					  ResultCount
					, ua.id
					)
			)
			.fetch();
			
info.addError( 1, "resultCount " + resultCount.size());
// return info;

// Class for storing records
class ResultBean {
    String		username
	Date		activationDate
	Boolean		active
    Long		id
	int			transIn
	int			transOut
    BigDecimal	totalIn
    BigDecimal	totalOut
	String		typeName
	
	private SimpleDateFormat	dateFormatter =  new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat	datetimeFormatter =  new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public int getIdInt()
	{
		return (int) this.id
	}
	
	public int getActiveFormatted()
	{
		return this.active ? 1 : 0;
	}
	
	public String getActivationDateFormatted()
	{
		return this.dateFormatter.format(this.activationDate)
	}
	
	public String getTotalFormatted()
	{
		return ((BigDecimal) (this.totalIn - this.totalOut)).setScale(2).toPlainString();
	}
	
	public String getTotalOutFormatted()
	{
		return this.totalOut.setScale(2).toPlainString();
	}
	
	public String getTotalInFormatted()
	{
		return this.totalIn.setScale(2).toPlainString();
	}
	
	public String getTransTotal()
	{
		return (this.transIn+this.transOut);
	}
}

// DBQuery query	= queryUA
			// .leftJoin(
				// entityManagerHandler.subQuery(tOut)
					// .where(tOut.date.lt(dateTo))
					// .groupBy(
						  // tOut.from
					// )
					// .select(
						  // tOut.from
						// , tOut.amount.sum().as("totalOut")
					// )
				// )
				// .on(	 tOut.from.eq(ua)
					// .and(tOut.date.lt(dateTo))
					// )
			// .leftJoin(tIn ).on(tIn.to.eq(ua).and(tIn.date.lt(dateTo)))
			// .orderBy(ua.user().username.asc())
			// .limit(isPAGE ? pageSize		: Integer.MAX_VALUE)
			// .offset(isPAGE ? currentPage*pageSize	: 0);

DBQuery query	= queryUA
			.leftJoin(t).on(
					t.from.eq(ua).or(t.to.eq(ua))
				,	t.date.lt(dateTo)
			)
			.groupBy(
				  ua.user().username
				, ua.user().activationDate
				, ua.id
				, ua.active
				, ua.type().name
			)
			.orderBy(ua.user().username.asc())
			// .limit(isPAGE ? pageSize		: Integer.MAX_VALUE)
			// .offset(isPAGE ? currentPage*pageSize	: 0);

def result = query.select(
				Projections.bean(
				  ResultBean
				, ua.user().username
				, ua.user().activationDate
				, ua.id
				, ua.active
				, ua.type().name.as("typeName")
				, SQLExpressions.sum(new CaseBuilder().when(t.to.eq(ua)).then(1).otherwise(0)).as("transIn")
				, SQLExpressions.sum(new CaseBuilder().when(t.from.eq(ua)).then(1).otherwise(0)).as("transOut")
				, new CaseBuilder().when(t.to.eq(ua)).then(t.amount).otherwise(0).sum().as("totalIn")
				, new CaseBuilder().when(t.from.eq(ua)).then(t.amount).otherwise(0).sum().as("totalOut")
				)
			); 


// evaluate columns to be displayed
def columns = [];


// if (fields.containsKey("index"))
	// columns <<[header: "#", property: "index", width: "2.5%", align: "right"];

if (fields.containsKey("name"))
	columns <<[header: fields.get("name").value, property: "username", width: "14%"];

if (fields.containsKey("activationDate"))
	columns <<[header: fields.get("activationDate").value, property: "activationDateFormatted", width: "10%"];

if (fields.containsKey("account"))
	columns <<[header: fields.get("account").value, property: "idInt", width: "8%"];

if (fields.containsKey("type"))
	columns <<[header: fields.get("type").value, property: "typeName", width: "15%"];

if (fields.containsKey("active"))
	columns <<[header: fields.get("active").value, property: "activeFormatted", width: "2%"];

if (fields.containsKey("transIn"))
	columns <<[header: fields.get("transIn").value, property: "transIn", width: "5%", align: "right"];

if (fields.containsKey("totalIn"))
	columns <<[header: fields.get("totalIn").value, property: "totalInFormatted", width: "10%", align: "right"];

if (fields.containsKey("transOut"))
	columns <<[header: fields.get("transOut").value, property: "transOut", width: "5%", align: "right"];

if (fields.containsKey("totalOut"))
	columns <<[header: fields.get("totalOut").value, property: "totalOutFormatted", width: "10%", align: "right"];

if (fields.containsKey("transTotal"))
	columns <<[header: fields.get("transTotal").value, property: "transTotal", width: "5%", align: "right"];

if (fields.containsKey("balance"))
	columns <<[header: fields.get("balance").value, property: "totalFormatted", width: "10%", align: "right"];


// Build the result
return [
    columns: columns,
	rows: result.fetch(),
	// ToDo:
	// totalCount: resultCount.size(),
]
