/**
 *
 * Generates a list of users.
 *
 * Script parameters:
 * 		none
 *
 * Included libraries:
 *		- libErrorHandling
 *
 * Custom form fields:
 * 		- Date			dateTo		Optional	Date part of point in time for the snapshot.
 *												Null yields in current date.
 *		- String		timeTo		Optional	Time part of the snapshot in the format
 *												"HH:mm".
 *												Null yields in "23:59".
 *												Seconds are always "59".
 *		- List			groups		Required	List of group's internal names.
 * 		- List			fields		Required	List of [index, name, activationDate, account, type,
 *												active, transIn, totalIn, transOut, totalOut, transTotal,
 *												balance]
 *
 * Returns
 *		org.cyclos.model.system.operations.CustomOperationPageResult:
 * 		//int			#			Optional	Line number
 * 		- String		Member		Default		Username of the account owner.
 *		- String		Activation Date Optional User activation date
 * 		- String 		Account		Optional	Account id
 * 		- String 		Type		Default		Account type
 * 		- Boolean		Active		Optional	Account active [0|1]
 *		- Int			In #		Optional	No. of incoming transactions
 * 		- BigDecimal	In £B		Optional	Sum of incoming transactions up to the given point in time
 *		- Int			Out #		Optional	No. of outgoing transactions
 * 		- BigDecimal	Out £B		Optional	Sum of incoming transactions up to the given point in time
 *		- Int			Total #		Optional	Total no. of transactions
 * 		- BigDecimal	Balance		Default		Account balance at the given point in time
 *												(see fields).
 *
 * Version:
 *		1.1, 2017-01-24
 *
 * Authors:
 *		- martin.rueegg@bristolpound.org
 *
 * Known issues:
 *		- CustomOperationPageContext not working (Cyclos)
 *
 **/


 //import
import com.querydsl.core.types.Projections
import com.querydsl.core.types.dsl.CaseBuilder
import com.querydsl.sql.SQLExpressions
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.time.DateUtils

import org.cyclos.entities.banking.QTransfer
import org.cyclos.entities.banking.QUserAccount
import org.cyclos.entities.system.CustomFieldPossibleValue
import org.cyclos.entities.users.QUser
import org.cyclos.entities.users.UserGroup
import org.cyclos.impl.utils.persistence.DBQuery
import org.cyclos.model.users.groups.UserGroupVO
import org.cyclos.model.utils.DatePeriodDTO
import org.cyclos.model.utils.ModelHelper
import org.cyclos.server.utils.DateHelper


info.addError( 1, "formParameters.userStartsWith " + formParameters.userStartsWith);
info.addError( 1, "isCSV " + isCSV);
info.addError( 1, "isPDF " + isPDF);
info.addError( 1, "isPAGE " + isPAGE);
info.addError( 1, "currentPage" + currentPage);
info.addError( 1, "pageSize" + pageSize);
info.addError( 1, "pageContext " + binding.pageContext);
// return info;

// prepare variables
def int			i 		= 0;


// evaluate selected groups
Boolean				allGroups	= false;
List<UserGroupVO>	groups		= [];

for (CustomFieldPossibleValue cf : formParameters.groups)
{
    if (allGroups == false)
    {
        if (cf.internalName.trim() == "all")
        {
            allGroups = true;
			groups		= [];
			
			def queryBG = new org.cyclos.model.users.groups.BasicGroupQuery([
				groupSet: 		new org.cyclos.model.users.groups.GroupSetVO([internalName: "memberAccounts"]),
				natures:		[org.cyclos.model.users.groups.BasicGroupNature.MEMBER_GROUP],
				currentPage:	0,
				pageSize:		Integer.MAX_VALUE,
			])
			
			info.addError( 1, "pre r ");
			
			groupService.search(queryBG).getPageItems().each
			{	
				r ->
				info.addError( 1, "r " + r.internalName);
				
			//	groups		<<r.clone(); //--> causes an error in accessing an non-persistent object ...
				groups		<<entityManagerHandler.find(UserGroup, r.internalName);
			}
        }
        else
        {
			groups		<<entityManagerHandler.find(UserGroup, cf.internalName.trim());
        }
    }
}

// evaluate selected fields
Map<String, CustomFieldPossibleValue> fields	= new HashMap<String, CustomFieldPossibleValue>();

for (CustomFieldPossibleValue cf : formParameters.fields)
{
	fields.put(cf.internalName, cf);
}

if (fields.size() == 0)
{
	return new ResultError(200, "At least one output field needs to be selected.");
}


def u		= QUser.user


class ResultCount {
    Long id
}


DBQuery queryU = entityManagerHandler
			.from(u)
			.where(
				  u.group().in(groups)
			)

List<ResultCount> resultCount = queryU
			.select(
				Projections.bean(
					  ResultCount
					, u.id
					)
			)
			.fetch();
			
info.addError( 1, "resultCount " + resultCount.size());
//return info;

// Class for storing records
class ResultBean {
    Long		uId
    String		username
	String		uFullName
	String		uEmail
	String		uPhone
	Date		uDateActivation
	// Date		uDateLastLogin
	
	private SimpleDateFormat	dateFormatter =  new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat	datetimeFormatter =  new SimpleDateFormat("yyyy-MM-dd HH:mm");

	public int getIdInt()
	{
		return (int) this.uId
	}
	
	public String getActivationDateFormatted()
	{
		return this.dateFormatter.format(this.uDateActivation);
	}
	
	// public String getLastLoginDateFormatted()
	// {
		// return this.datetimeFormatter.format(this.uDateLastLogin)
	// }
}

DBQuery query	= queryU
			.orderBy(u.username.asc())
			// .limit(isPAGE ? pageSize		: Integer.MAX_VALUE)
			// .offset(isPAGE ? currentPage*pageSize	: 0);

def result = query.select(
				Projections.bean(
				  ResultBean
				, u.id.as("uId")
				, u.username
				, u.name.as("uFullName")
				, u.email.as("uEmail")
				// , (String) u.phones().normalizedNumber.as("uPhone")
				, u.activationDate.as("uDateActivation")
				)
			); 


// evaluate columns to be displayed
def columns = [];


if (fields.containsKey("id"))
	columns <<[header: "ID", property: "idInt", width: "2.5%", align: "right"];

if (fields.containsKey("username"))
	columns <<[header: fields.get("username").value, property: "username", width: "14%"];

if (fields.containsKey("nameFull"))
	columns <<[header: fields.get("nameFull").value, property: "uFullName", width: "14%"];

if (fields.containsKey("email"))
	columns <<[header: fields.get("email").value, property: "uEmail", width: "14%"];

if (fields.containsKey("dateActivation"))
	columns <<[header: fields.get("dateActivation").value, property: "activationDateFormatted", width: "10%"];

// if (fields.containsKey("dateLogin"))
	// columns <<[header: fields.get("dateLogin").value, property: "lastLoginDateFormatted", width: "10%"];

// info.addError( 1, "resultCount " + result.fetch().size());
// return info;



// Build the result
return [
    columns: columns,
	rows: result.fetch(),
	// ToDo:
	// totalCount: resultCount.size(),
]
