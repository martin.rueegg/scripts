/**
 *
 * Essential library for scripts
 *
 * Version:
 *		0.1, 2016-08-23
 *
 * Script parameters:
 * 		none
 *
 * Included libraries:
 *		- none
 *
 * Authors:
 *		- martin.rueegg@bristolpound.org
 *
 * Known issues:
 *		none so far
 *
 **/

package org.bristolpound.cyclos.scripting.library.essentials

// import
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.lang.SystemUtils
import org.apache.xalan.xsltc.compiler.Import;
import org.cyclos.model.utils.DateTime;
//import org.bristolpound.cyclos.scripting.library.records.ResultRow;
//import org.bristolpound.cyclos.scripting.library.errorHandling.ResultError;

if (!SystemUtils.isJavaVersionAtLeast(1.8))
{
	return new ResultError(100, "This script requires at least Java version 1.8, however, Java " + System.getProperty("java.version") + " is used.");
}

/**
 * Utilities for conversion between the old and new JDK date types 
 * (between {@code java.util.Date} and {@code java.time.*}).
 * 
 * <p>
 * All methods are null-safe.
 *
 * @link http://stackoverflow.com/a/27378709
 *
 */
public class DateConvertUtils {

    /**
     * Calls {@link #asLocalDate(Date, ZoneId)} with the system default time zone.
     */
    public static LocalDate asLocalDate(java.util.Date date) {
        return asLocalDate(date, ZoneId.systemDefault());
    }

    /**
     * Creates {@link LocalDate} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static LocalDate asLocalDate(java.util.Date date, ZoneId zone) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Date)
            return ((java.sql.Date) date).toLocalDate();
        else
            return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDate();
    }

    /**
     * Calls {@link #asLocalDateTime(Date, ZoneId)} with the system default time zone.
     */
    public static LocalDateTime asLocalDateTime(java.util.Date date) {
        return asLocalDateTime(date, ZoneId.systemDefault());
    }

    /**
     * Creates {@link LocalDateTime} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static LocalDateTime asLocalDateTime(java.util.Date date, ZoneId zone) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Timestamp)
            return ((java.sql.Timestamp) date).toLocalDateTime();
        else
            return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDateTime();
    }

    /**
     * Calls {@link #asUtilDate(Object, ZoneId)} with the system default time zone.
     */
    public static java.util.Date asUtilDate(Object date) {
        return asUtilDate(date, ZoneId.systemDefault());
    }

    /**
     * Creates a {@link java.util.Date} from various date objects. Is null-safe. Currently supports:<ul>
     * <li>{@link java.util.Date}
     * <li>{@link java.sql.Date}
     * <li>{@link java.sql.Timestamp}
     * <li>{@link java.time.LocalDate}
     * <li>{@link java.time.LocalDateTime}
     * <li>{@link java.time.ZonedDateTime}
     * <li>{@link java.time.Instant}
     * </ul>
     * 
     * @param zone Time zone, used only if the input object is LocalDate or LocalDateTime.
     * 
     * @return {@link java.util.Date} (exactly this class, not a subclass, such as java.sql.Date)
	 * 
     */
    public static java.util.Date asUtilDate(Object date, ZoneId zone) {
        if (date == null)
            return null;

        if (date instanceof java.sql.Date || date instanceof java.sql.Timestamp)
            return new java.util.Date(((java.util.Date) date).getTime());
        if (date instanceof java.util.Date)
            return (java.util.Date) date;
        if (date instanceof LocalDate)
            return java.util.Date.from(((LocalDate) date).atStartOfDay(zone).toInstant());
        if (date instanceof LocalDateTime)
            return java.util.Date.from(((LocalDateTime) date).atZone(zone).toInstant());
        if (date instanceof ZonedDateTime)
            return java.util.Date.from(((ZonedDateTime) date).toInstant());
        if (date instanceof Instant)
            return java.util.Date.from((Instant) date);

        throw new UnsupportedOperationException("Don't know hot to convert " + date.getClass().getName() + " to java.util.Date");
    }

    /**
     * Creates an {@link Instant} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static Instant asInstant(Object date)
	{
		return asInstant(date, ZoneId.systemDefault());
	}

    public static Instant asInstant(Object date, ZoneId zone)
	{
        if (date == null)
            return null;

        if (date instanceof Instant)
		{
            return (Instant) date;
		}
		
        if (
				date instanceof java.util.Date 
			||  date instanceof java.sql.Date
			||  date instanceof java.sql.Timestamp
			)
		{
            return Instant.ofEpochMilli(((java.util.Date) date).getTime());
		}
		
        if (date instanceof LocalDate)
		{
            return ((LocalDate) date).atStartOfDay(zone).toInstant();
		}
		
        if (
				date instanceof LocalDateTime
			||	date instanceof ZonedDateTime
			)
		{
            return ((LocalDateTime) date).toInstant();
		}
		
		if (date instanceof LocalTime)
		{
			return Instant.from(date);
		}

        throw new UnsupportedOperationException("Don't know how to convert " + date.getClass().getName() + " to java.time.Instant");

    }

    /**
     * Calls {@link #asZonedDateTime(Date, ZoneId)} with the system default time zone.
     */
    public static ZonedDateTime asZonedDateTime(Date date) {
        return asZonedDateTime(date, ZoneId.systemDefault());
    }

    /**
     * Creates {@link ZonedDateTime} from {@code java.util.Date} or it's subclasses. Null-safe.
     */
    public static ZonedDateTime asZonedDateTime(Date date, ZoneId zone) {
        if (date == null)
            return null;
        else
            return asInstant(date).atZone(zone);
    }

}


public class MyDate {
	
	Instant i;
	ZoneId defaultZone = ZoneId.systemDefault();
	
	MyDate()
	{
	}
	
	MyDate(date)
	{
		this.i = DateConvertUtils.asInstant(date);
	}
	
	public MyDate atStartOfDay()
	{
		return this.atStartOfDay(this.defaultZone);
	}
	
	public MyDate atStartOfDay(ZoneId zone)
	{
		return 	( this.i 
				? new MyDate(this.i.atZone(zone).toLocalDate().atStartOfDay())
				: new MyDate()
				)
	}
	
	public MyDate clone()
	{
		return new MyDate(this);
	}
	
	public boolean isNull()
	{
		return (i == null);
	}
	
	public boolean isToday()
	{
		return this.isToday(this.defaultZone);
	}
	
	public boolean isToday(ZoneId zone)
	{
		if (this.i == null)
			return false;
		
		LocalDate localDate = this.toLocalDate();
		
		if (localDate == null)
			return false;
		
		return localDate.equals(LocalDate.now());
	}
	
	public MyDate setTimeTo()
	{
		return this;
	}
	
	public Instant toInstant()
	{
		return this.i;
	}
	
	public ZonedDateTime toZonedDateTime(ZoneId zone)
	{
		if (this.i == null) return null
		
		return this.i.atZone(zone);
	}
	
	public LocalDate toLocalDate()
	{
		return this.toLocalDate(this.defaultZone);
	}
	
	public LocalDate toLocalDate(ZoneId zone)
	{
		if (this.i == null) return null
		
		return this.toZonedDateTime(zone)?.toLocalDate();
	}
	
	public LocalDateTime toLocalDateTime()
	{
		return this.toLocalDateTime(this.defaultZone);
	}
	
	public LocalDateTime toLocalDateTime(ZoneId zone)
	{
		if (this.i == null) return null
		
		return this.toZonedDateTime(zone)?.toLocalDateTime();
	}
	
	public LocalDateTime getLocalStartOfDay()
	{
		return this.getLocalStartOfDay(this.defaultZone);
	}
	
	public LocalDateTime getLocalStartOfDay(ZoneId zone)
	{
		if (this.i == null) return null
		
		return this.toLocalDate(zone).atStartOfDay();
	}
	
	public LocalDateTime getLocalEndOfDay()
	{
		return this.getLocalEndOfDay(this.defaultZone);
	}
	
	public LocalDateTime getLocalEndOfDay(ZoneId zone)
	{
		if (this.i == null) return null
		
		return this.getLocalStartOfDay(zone).plusDays(1).minusNanos(1);
	}
	
	public LocalTime getLocalTime()
	{
		return this.getLocalTime(this.defaultZone);
	}
	
	public LocalTime getLocalTime(ZoneId zone)
	{
		if (this.i == null) return null
		
		return this.getZonedDateTime(zone).toLocalTime();
	}
	
	public Date toDate()
	{
		return toDate(this.defaultZone);
	}
	
	public Date toDate(ZoneId zone)
	{
		if (this.i == null) return null
		
		return DateConvertUtils.asUtilDate(this.i, zone);
	}
	
	public MyDate setTimeToNow()
	{
		return setTimeToNow(this.defaultZone);
	}
	
	public MyDate setTimeToNow(ZoneId zone)
	{
		if (this.i == null)
		{
			this.i = Instant.now();
		}
		else
		{
			this.i = (this.toLocalDate(zone)?:LocalDate.now()).atTime(LocalTime.now(zone))?.toInstant();
		}
		
		return this;
	}
	
	public MyDate setTimeToEndOfDay()
	{
		return this.setTimeToEndOfDay(this.defaultZone);
	}
	
	public MyDate setTimeToEndOfDay(ZoneId zone)
	{
		if (this.i == null) return this
		
		this.i = this.getLocalEndOfDay(zone).toInstant();
		
		return this;
	}
	
	public MyDate setTimeToStartOfDay()
	{
		return this.setTimeToStartOfDay(this.defaultZone);
	}
	
	public MyDate setTimeToStartOfDay(ZoneId zone)
	{
		if (this.i == null) return this
		
		this.i = this.getLocalStartOfDay(zone).toInstant();
		
		return this;
	}
	
	public MyDate setToNow()
	{
		this.i = Instant.now();
		
		return this;
	}
	
	public org.cyclos.model.utils.DateTime toDateTime()
	{
		return this.toCyclosDateTime(this.defaultZone);
	}
	
	public org.cyclos.model.utils.DateTime toDateTime(ZoneId zone)
	{
		if (this.i == null) return null
		
		return new org.cyclos.model.utils.DateTime(this.toZonedDateTime(zone));
	}
	
	public static MyDate of(date)
	{
		return new MyDate(date);
	}
	
	public static MyDate now()
	{
		return new MyDate(Instant.now());
	}
	
	public static MyDate atStartOfDay(java.util.Date date)
	{
		return new MyDate(date).atStartOfDay();
	}
	
	public static ZoneId getSystemDefaultZone()
	{
		return ZoneId.of(ZoneId.systemDefault().toString());
	}
}