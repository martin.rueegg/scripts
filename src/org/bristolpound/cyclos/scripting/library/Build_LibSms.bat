@echo off
setlocal enabledelayedexpansion

set output=%~dp0\LibSms.static.groovy
call %~dp0\Build_lib_header.bat

echo libSmsHsl.groovy
echo. 								>>%output%
cat %~dp0\libSmsHsl.groovy			>>%output%

echo libSmsPlivo.groovy
echo. 								>>%output%
cat %~dp0\libSmsPlivo.groovy		>>%output%

echo libWsSMS.groovy
echo. 								>>%output%
cat %~dp0\libWsSMS.groovy			>>%output%

call Build_LibWs.bat
