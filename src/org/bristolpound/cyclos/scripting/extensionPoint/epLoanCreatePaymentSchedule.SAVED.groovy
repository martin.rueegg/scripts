import org.cyclos.entities.banking.PaymentTransferType
import org.cyclos.entities.banking.Transaction
import org.cyclos.entities.banking.Transfer
import org.cyclos.entities.users.RecordCustomFieldPossibleValue
import org.cyclos.entities.users.SystemRecord
import org.cyclos.entities.users.UserRecord
import org.cyclos.entities.users.UserRecordType


Transfer		tf							= transfer;
Transaction		ta							= tf.getTransaction();
Date			tfDate						= tf.getDate();
Map				taFields					= scriptHelper.wrap(ta);
SystemRecord	loanType					= taFields['loanType'];
Map				ltFields					= scriptHelper.wrap(loanType);

int				defaultNoOfInstallments		= ltFields['defaultNoOfInstallments'];
int				defaultPaymentIntervalCount	= ltFields['defaultPaymentIntervalCount'];
String			defaultPaymentIntervalUnit	= ltFields['defaultPaymentIntervalUnit'].getValue();

println defaultPaymentIntervalUnit;

UserRecordType	urtLoanRepaymentSchedule	= editOrCreate(UserRecordType, 'loanRepaymentSchedule');
RecordCustomFieldPossibleValue rcfpvTypeLoanRepaymentDue = editOrCreate(
	RecordCustomFieldPossibleValue
	, 'loanRepaymentSchedule.type.repaymentDue'
);


for (i in 1..defaultNoOfInstallments)
{
	println i;

	UserRecord urLoanRepaymentSchedule =
		editOrCreate(urtLoanRepaymentSchedule, [
		type			: rcfpvTypeLoanRepaymentDue,
		defaultPaymentIntervalCount	: 1,
		defaultPaymentIntervalUnit	: rcfpvLoanTypePeriodUnitMonths, // 'MONTHS',
		defaultAPR						: 0,
		defaultDelayAPR					: 10,
		defaultMissedPaymentFee			: 25,
		defaultReschedulingFee			: 15,
	]);
}

assert loanType instanceof Map

