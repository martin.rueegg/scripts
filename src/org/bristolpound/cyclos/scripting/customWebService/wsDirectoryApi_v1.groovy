/*
 *===========================================
 *------  wsDirectoryApi_v1  ----------------
 *===========================================
 *
 * @version: 1.1.7
 *
 **********************************************
 * Custom web service details
 * --------------------------
 *
 * @httpMethod:
 *  Both
 *
 * @runAs:
 *  Guest
 *
 * @scriptParameters:
 *
 * @urlMappings:
 *  directory/v1
 *  directory/v1/{sub}
 *  directory/v1/{sub}/**
 *  directory/v1.{minor}
 *  directory/v1.{minor}/{sub}
 *  directory/v1.{minor}/{sub}/**
**/

import java.util.Map


return new WsDirectoryRoot
(
	scriptBindingBP
	, '1.1.6'
	, this.&getUsers
	, [
		addressMapping: (Map<String, String>)
		[
			id:             "id",
			name:           "name",
			addressLine1:   "addressLine2",
			buildingNumber: "buildingNumber",
			street:         "street",
			addressLine3:   "neighborhood",
			postcode:       "zip",
			townOrCity:     "city",
			location:       "location",
		],

		staticFields: [
			new WsField(
				  "username"
				, "Username"
				, "STRING"
				, this.&valueFromUsername
			),

			new WsField(
				  "activationDate"
				, "Joined"
				, "DATE"
				, this.&valueFromActivationDate
			),

			new WsField(
				  "icon"
				, "Icon"
				, "INTEGER"
				, this.&valueFromIcon
				, new WsFieldOptions(
					description:        "To be used to represent the location on a map.",
					multiSelect:        false,
					baseUrl:            sessionData.configuration.fullUrl,
				)
				, new WsFieldPossibleValues([
					0: "default.jpg",
					1: "cash1.jpg",
					2: "cash3.jpg",
					3: "atm.jpg",
				])
			),

			new WsField(
				"image"
				, "Member Image"
				, "IMAGE"
				, this.&valueFromImage
				, new WsFieldOptions(baseUrl: sessionData.configuration.fullUrl + "/api/images/profile/")
			),

			new WsField(
				  "addresses"
				, "Addresses"
				, "ADDRESS"
				, this.&valueFromAddresses
			),
		],

		profileFields: [
			"bcumembernumber":      new WsFieldIgnore(),
			"contractAddressZIP":   new WsFieldIgnore(),
			"friendlyUrl":          new WsFieldIgnore(),
			"bpFeatures":           new WsFieldOptions(baseUrl: sessionData.configuration.fullUrl),
		],

		groupings: [
			WsGroupServices,
			WsGroupIcons,
			WsGroupCategoryGroups_businesscategory,
			WsGroupCategories_businesscategory,
		],
	],
).evalRequest();



/////////////////////////////
// End of script
/////////////////////////////


// function and class definitions

protected getUsers()
{
	java.util.Collection<? extends Group> groups        = [];

//  def queryBG = new org.cyclos.model.users.groups.BasicGroupQuery([
//      groupSet:       new org.cyclos.model.users.groups.GroupSetVO([internalName: "memberAccounts"]),
//      natures:        [org.cyclos.model.users.groups.BasicGroupNature.MEMBER_GROUP],
//      currentPage:    0,
//      pageSize:       Integer.MAX_VALUE,
//  ])
//
//  for (UserGroupVO g: ScriptBindingBP.binding.groupService.search(queryBG).getPageItems())
//  {
//      groups      << ScriptBindingBP.entityManagerHandler.find(UserGroup, g.internalName);
//  }

	groups      << binding.entityManagerHandler.find(UserGroup, "accesspointproviders");
	groups      << binding.entityManagerHandler.find(UserGroup, "businessmembers");

	return binding.userService.iterateByGroups(groups);
}

public String valueFromUsername(WsDirectoryEntry entry)
{
	return entry.u.username
}

public String valueFromActivationDate(WsDirectoryEntry entry)
{
	LocalDateTime ldt = LocalDateTime.ofInstant(entry.getActivationDate().toInstant(), ZoneId.systemDefault()).minusHours(12);
	return ldt.minusDays(ldt.getDayOfWeek().value + 6).toLocalDate().toString();
}

public int valueFromIcon(WsDirectoryEntry entry)
{
	return (entry.u.group.internalName == "accesspointproviders" ? 1 : 0)
}

public Object valueFromImage(WsDirectoryEntry entry)
{
	return entry.getImage();
}

public Object valueFromAddresses(WsDirectoryEntry entry)
{
	return entry.getAddresses();
}


class WsGroupServices
	extends WsGroupMultiSelect
{
	WsGroupServices(WsGroupings parent)
	{
		super(parent);

		this.id                 =   "services";
		this.label              =   "Services";
		this.field              =   [$ref: "\$.fields.bpFeatures"];
		this.options            =
		[
			multiSelect         :   true,
			baseUrl             :   context.sessionData.configuration.fullUrl,
		];
	}
}

class WsGroupIcons
	extends WsGroupMultiSelect
{
	WsGroupIcons(WsGroupings parent)
	{
		super(parent);

		this.id                 = "icon";
		this.field              = [$ref: "\$.fields.icon"];
		this.options            = [
			multiSelect         : true,
			];
	}
}

class WsGroupCategoryGroups_businesscategory
	extends WsGroupCategoryGroups
{
	WsGroupCategoryGroups_businesscategory(WsGroupings parent)
	{
		super(parent
			, "categoryGroups"
			, "Category Groups"
			, "businesscategory"
			);
	}
}

class WsGroupCategories_businesscategory
	extends WsGroupCategories
{
	WsGroupCategories_businesscategory(WsGroupings parent)
	{
		super(parent
			, "categories"
			, "Categories"
			, "businesscategory"
			);
	}
}

