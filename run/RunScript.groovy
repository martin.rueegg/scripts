import org.cyclos.entities.users.BasicUser
import org.cyclos.impl.InvocationContext
import org.cyclos.impl.InvokerHandler
import org.cyclos.impl.access.DirectUserSessionData
import org.cyclos.impl.access.ScriptSessionData
import org.cyclos.impl.access.SessionData
import org.cyclos.impl.system.ScriptHelper
import org.cyclos.impl.utils.persistence.RawEntityManagerHandler
import org.cyclos.model.utils.TransactionLevel
import org.cyclos.server.utils.IOHelper
import org.cyclos.server.utils.PropertiesHelper
import org.springframework.context.support.ClassPathXmlApplicationContext

// Parser for command-line arguments
def cli = new CliBuilder(usage: "RunScript <options>", header: "Options:")
cli.network(args: 1, "The network internal name. Default: run in global mode")
cli.config(args: 1, "The cyclos.properties file to use."
    + " Default: cyclos.properties in the current directory.")
cli.init(args: 1, "The script that will return a Map with bindings to emulate the real execution of the script."
    + " Default: no initialization script.")
cli.params(args: 1, "The properties file passed to the script as scriptParameters."
    + " Default: no parameters.")
cli.script(args: 1, required: true, "The script file to run. Required.")
def options = cli.parse(args)
if (!options) {
    System.exit(10)
}

// Read the script file
def scriptFile = new File(options.script)
if (!scriptFile.exists()) {
    println("Script file not found: ${scriptFile.absolutePath}")
    System.exit(11)
}

// Read the initialization file
def initFile = options.init ? new File(options.init) : null
if (initFile != null && !initFile.exists()) {
    println("Initialization script file not found: ${initFile.absolutePath}")
    System.exit(12)
}

// Read the parameters file
def paramsFile = options.params ? new File(options.params) : null
if (paramsFile != null && !paramsFile.exists()) {
    println("Parameters file not found: ${paramsFile.absolutePath}")
    System.exit(13)
}

// Read the configuration file
def configStr = options.config ?: 'cyclos.properties'
def configFile = new File(configStr)
def configResource = this.class.class.getResourceAsStream("/$configStr")
IOHelper.close(configResource)
if (!configFile.exists() && configResource == null) {
    println("Configuration file not found: ${configFile.absolutePath}")
    System.exit(14)
}

// Show the configuration arguments
println("""Running with arguments:
* Network: ${options.network ?: '<global mode>'}
* Configuration file: ${configResource == null ? configFile.absolutePath : "<classpath resource: ${configStr}>"}
* Script file: ${scriptFile.absolutePath}
* Initialization script file: ${initFile?.absolutePath ?: '<none>'}
* Parameters file: ${paramsFile?.absolutePath ?: '<none>'}
""")
int i = 0
print("Initialize the Spring context ...")
// Initialize the Spring context
System.setProperty("cyclos.maxBackgroundTasks", "0");
System.setProperty("cyclos.properties", configResource != null ? "/${configStr}" : configFile.getAbsolutePath())
System.setProperty("file.encoding", "UTF-8")
def appCtx = new ClassPathXmlApplicationContext("spring/bootstrap.xml", "spring/services.xml")
println(" Done.")

try {
	print("Initialize handlers ...")
    def scriptHelper = appCtx.getBean(ScriptHelper.class)
    def invokerHandler = appCtx.getBean(InvokerHandler.class)
    def rawEntityManagerHandler = appCtx.getBean(RawEntityManagerHandler.class)
    def scriptParameters = paramsFile ? PropertiesHelper.from(new FileInputStream(paramsFile)) : [:]
	println(" Done.")
	
    // Load the script and run in a transaction
    def result = invokerHandler.runAsInTransaction(InvocationContext.system(), TransactionLevel.READ_WRITE) { status ->
println(i++)
        def entityManager = rawEntityManagerHandler.entityManager
println(i++)
        def shell = new GroovyShell()
println(i++)
		
        // When running in a network, initialize the corresponding session data. Otherwise, global system
        def sessionData = InvocationContext.system()
println(i++)
        if (options.network) {
            def network = entityManager.createQuery("from Network n where n.internalName = :net")
                .setParameter('net', options.network)
                .singleResult
            sessionData = InvocationContext.system(network)
        }
        
println(i++)
        // Helper to run a script with the current context
        def runScript = { file, extra ->
            def script = shell.parse(file)
            
            // Initialize the script binding with each exposed bean to scripts
            script.binding.scriptHelper = scriptHelper
            scriptHelper.beanMap.forEach(script.binding.&setVariable)
            script.binding.entityManager = entityManager
            script.binding.formatter = InvocationContext.getFormatter()
            script.binding.scriptParameters = scriptParameters
            extra.forEach(script.binding.&setVariable)
            if (extra.sessionData instanceof SessionData) {
                sessionData = extra.sessionData
            } else if (extra.runAs instanceof BasicUser) {
                sessionData = new DirectUserSessionData(extra.runAs, sessionData)
            }
println(i++)
            script.binding.sessionData = new ScriptSessionData(sessionData, extra.runAsSystem == true)
            
println(i++)
            return invokerHandler.runAs(script.binding.sessionData) {
                return script.run()
            }
        }
        
println(i++)
        // Run the initialization script, if any
        def extra = initFile ? runScript(initFile, [:]) ?: [:] : [:]
        
println(i++)
        // Run the real script
        println()
        return runScript(scriptFile, extra) as String
    }
    println("\nReturn value from script:\n${result}")
} finally {
    IOHelper.close(appCtx)
}
System.exit(0)
